package com.nearmotion.indoorsensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.annotation.Nullable;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.util.Date;


/**
 * Algorithm for calculating distance increment latitude and longitude base on sensor information.
 *
 * Created by Majid Aljishi on 1/14/2018.
 */

public class SensorsDataProvider implements SensorEventListener {

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mMagnetometer;
    private Sensor mLinearAcceleration;
    private ReactContext mReactContext;
    public Arguments mArguments;

    private float[] AccelerometerValue;
    private float[] MagnetometerValue;

    private Float currentAcceleration = 0.0F;
    private Float  currentDirection = 0.0F;
    private Float CurrentSpeed = 0.0F;
    private Float CurrentDistanceTravelled = 0.0F;

    private float[] speed;
    private float prevTime, currentTime, changeTime,distanceY,distanceX,distanceZ;
    private float[] currentVelocity;

    private Float prevAcceleration = 0.0F;
    private Float prevSpeed = 0.0F;
    private Float prevDistance = 0.0F;
    private Float totalDistance;
    private Boolean First, FirstSensor = true;


    private Double earthRadius = 6378D;
    private Double initialLatitude, initialLongitude;
    private Boolean IsFirst = true;

    private Date CollaborationWithGPSTime;

    private boolean startCheck = false;

    final float ALPHA = 0.25f; // if ALPHA = 1 OR 0, no filter applies.
    private float bearingFix = 0.0F;
    private boolean enableLowPass = true;

    public SensorsDataProvider(ReactApplicationContext reactContext) {

        First = FirstSensor = true;
        currentVelocity = new float[3];
        speed = new float[3];
        totalDistance = 0.0F;

        mReactContext = reactContext;

        // Initialize Sensor Manager
        mSensorManager = (SensorManager) reactContext.getSystemService(reactContext.SENSOR_SERVICE);

        // Initialize Sensors
        mAccelerometer = mSensorManager != null ?
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) : null;

        mMagnetometer = mSensorManager != null ?
                mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) : null;

        mLinearAcceleration = mSensorManager != null ?
                mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION) : null;

        Log.d(SensorsDataProvider.class.getSimpleName(), "Indoor Sensor Data Provider Created");
    }

    /**
     * Method to start sensors listener.
     */
    public void start() {
        if (mAccelerometer!=null && mLinearAcceleration!=null && mMagnetometer!=null) {
            mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
            mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_NORMAL);
            mSensorManager.registerListener(this, mLinearAcceleration, SensorManager.SENSOR_DELAY_NORMAL);
            Log.d(SensorsDataProvider.class.getSimpleName(), "Sensor Manager: listener registered.");
            startCheck = true;
        } else {
            Log.d(SensorsDataProvider.class.getSimpleName(), "Sensor Manager: sensors are null");
        }
    }

    /**
     * @return false by default you have to start to check.
     * Method to check status.
     */
    public boolean isStarted() {
        if (startCheck) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method to stop sensor listener.
     */
    public void stop() {
        mSensorManager.unregisterListener(this);
        startCheck = false;
        Log.d(SensorsDataProvider.class.getSimpleName(), "Sensor Manager: listener unRegistered.");
    }

    /**
     * Method to set initial latitude.
     */
    public void setInitialLatitude(Double val) {
        initialLatitude = val;
    }

    /**
     * Method to set initial longitude.
     */
    public void setInitialLongitude(Double val) {
        initialLongitude = val;
    }

    /**
     * Sensor event listener.
     */
    @Override
    public void onSensorChanged(SensorEvent event) {

        float[] values = event.values;
        Sensor mSensor = event.sensor;

        /* ACCELEROMETER */
        if(mSensor.getType() == Sensor.TYPE_ACCELEROMETER){
            if (enableLowPass) {
                AccelerometerValue = lowPass(values, AccelerometerValue);
            } else {
                AccelerometerValue = values;
            }
        }

        /* LINEAR ACCELERATION */
        if(mSensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION){
            if(First) {
                prevTime = event.timestamp / 1000000000;
                First = false;
                currentVelocity[0] = currentVelocity[1] = currentVelocity[2] = 0;
                distanceX = distanceY= distanceZ = 0;
            } else {
                currentTime = event.timestamp / 1000000000.0f;
                changeTime = currentTime - prevTime;
                prevTime = currentTime;

                calculateDistance(event.values, changeTime);

                currentAcceleration =  (float) Math.sqrt(event.values[0] * event.values[0] + event.values[1] * event.values[1] + event.values[2] * event.values[2]);

                CurrentSpeed = (float) Math.sqrt(speed[0] * speed[0] + speed[1] * speed[1] + speed[2] * speed[2]);
                CurrentDistanceTravelled = (float) Math.sqrt(distanceX *  distanceX + distanceY * distanceY +  distanceZ * distanceZ);
                CurrentDistanceTravelled = CurrentDistanceTravelled / 1000;

                if(FirstSensor){
                    prevAcceleration = currentAcceleration;
                    prevDistance = CurrentDistanceTravelled;
                    prevSpeed = CurrentSpeed;
                    FirstSensor = false;
                }
            }
        }

        /* MAGNETIC FIELD */
        if(mSensor.getType() == Sensor.TYPE_MAGNETIC_FIELD){
            if (enableLowPass) {
                MagnetometerValue = lowPass(values, MagnetometerValue);
            } else {
                MagnetometerValue = values;
            }

        }

        if(currentAcceleration != prevAcceleration ||
                CurrentSpeed != prevSpeed ||
                prevDistance != CurrentDistanceTravelled){

            if(!FirstSensor)
                totalDistance = totalDistance + CurrentDistanceTravelled * 1000;
            if (AccelerometerValue != null && MagnetometerValue != null && currentAcceleration != null) {
                //Direction
                float RT[] = new float[9];
                float I[] = new float[9];
                boolean success = SensorManager.getRotationMatrix(RT, I, AccelerometerValue,
                        MagnetometerValue);
                if (success) {
                    float orientation[] = new float[3];
                    SensorManager.getOrientation(RT, orientation);
                    float azimut = (float) Math.round(Math.toDegrees(orientation[0]));
                    currentDirection = ( azimut + bearingFix + 360) % 360;
                    if( CurrentSpeed > 0.2){
                        calculateCoordinates(currentAcceleration,CurrentSpeed,CurrentDistanceTravelled,currentDirection);
                    }
                }
                prevAcceleration = currentAcceleration;
                prevSpeed = CurrentSpeed;
                prevDistance = CurrentDistanceTravelled;
            }
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}

    /**
     * Method to calculate distance increment
     */
    private void calculateDistance (float[] acceleration, float deltaTime) {
        float[] distance = new float[acceleration.length];

        for (int i = 0; i < acceleration.length; i++) {
            speed[i] = acceleration[i] * deltaTime;
            distance[i] = speed[i] * deltaTime + acceleration[i] * deltaTime * deltaTime / 2;
        }
        distanceX = distance[0];
        distanceY = distance[1];
        distanceZ = distance[2];
    }

    /**
     * Method to calculate new latitude and longitude.
     */
    private void calculateCoordinates(Float currentAcceleration, Float currentSpeed,
                                      Float currentDistanceTravelled, Float currentDirection) {

        if(IsFirst){
            CollaborationWithGPSTime = new Date();
            Log.d(SensorsDataProvider.class.getSimpleName(), "First SensorsModule Reading");
            GetData(initialLongitude, initialLatitude,currentDistanceTravelled * 1000,
                    currentAcceleration, currentSpeed, currentDirection, "GPS");
            IsFirst  = false;
            return;
        }

        Date CurrentDateTime = new Date();

        if(CurrentDateTime.getTime() - CollaborationWithGPSTime.getTime() > 900000){
            //This IF Statement is to Collaborate with GPS position --> For accuracy --> 900,000 == 15 minutes
            GetData(initialLongitude, initialLatitude,currentDistanceTravelled * 1000,
                    currentAcceleration, currentSpeed, currentDirection, "GPS");
            return;
        }

        //Convert Variables to Radian for the Formula
        initialLatitude = Math.PI * initialLatitude / 180;
        initialLongitude = Math.PI * initialLongitude / 180;
        currentDirection = (float) (Math.PI * currentDirection / 180.0);

        //Formulae to Calculate the New Latitude and New Longitude
        Double newLatitude =
                Math.asin(Math.sin(initialLatitude) * Math.cos(currentDistanceTravelled / earthRadius) +
                        Math.cos(initialLatitude) * Math.sin(currentDistanceTravelled / earthRadius) * Math.cos(currentDirection));

        Double newLongitude =
                initialLongitude +
                        Math.atan2(Math.sin(currentDirection) * Math.sin(currentDistanceTravelled / earthRadius)
                                * Math.cos(initialLatitude), Math.cos(currentDistanceTravelled / earthRadius)
                                - Math.sin(initialLatitude) * Math.sin(newLatitude));

        //Convert Back from radians
        newLatitude = 180 * newLatitude / Math.PI;
        newLongitude = 180 * newLongitude / Math.PI;
        currentDirection = (float) (180 * currentDirection / Math.PI);

        //Update old Latitude and Longitude
        initialLatitude = newLatitude;
        initialLongitude = newLongitude;

        IsFirst = false;


        //Send data
        GetData(newLongitude,newLatitude,currentDistanceTravelled * 1000,
                currentAcceleration, currentSpeed, currentDirection, "Sensor");


    }

    /**
     * You can fix bearing value as you wish.
     * @param bearingFix
     */
    public void setBearingFix(float bearingFix) {
        this.bearingFix = bearingFix;
    }

    /**
     * Low pass filter
     * @param input
     * @param output
     * @return
     */
    protected float[] lowPass( float[] input, float[] output ) {

        if ( output == null ) return input;
        for ( int i=0; i<input.length; i++ ) {
            output[i] = output[i] + ALPHA * (input[i] - output[i]);
        }

        return output;
    }

    /**
     * Disable low pass filter
     */
    public void disableLowPass() {
        enableLowPass = false;
    }

    /**
     * Map Values to React Native
     *
     * @param newLatitude { Map Lat }
     * @param newLongitude { Map Lon }
     * @param currentDistance { Distance from coordinates }
     * @param currentAcceleration { Accelerating Value }
     * @param currentSpeed { Moving Speed }
     * @param currentDirection { Bearing Value }
     * @param dataType { Sensor, GPS}
     */

    private void GetData(Double newLatitude,
                         Double newLongitude,
                         Float currentDistance,
                         Float currentAcceleration,
                         Float currentSpeed,
                         Float currentDirection,
                         String dataType) {

        WritableMap map = mArguments.createMap();

        map.putString("type", dataType);
        map.putDouble("newLatitude", newLatitude);
        map.putDouble("newLongitude", newLongitude);
        map.putDouble("currentDistance", currentDistance);
        map.putDouble("currentAcceleration", currentAcceleration);
        map.putDouble("currentSpeed", currentSpeed);
        map.putDouble("currentDirection", currentDirection);

        sendEvent("SensorData", map);
    }

    /**
     * Sending Events to JavaScript
     */
    private void sendEvent(String eventName, @Nullable WritableMap params)
    {
        try {
            mReactContext
                    .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                    .emit(eventName, params);
        } catch (RuntimeException e) {
            Log.e(SensorsDataProvider.class.getSimpleName(),
                    "java.lang.RuntimeException:" +
                            " Trying to invoke JS before CatalystInstance has been set!");
        }
    }



}
