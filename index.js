'use strict'
//
//import { NativeModules } from 'react-native'
//// name as defined via ReactContextBaseJavaModule's getName
//module.exports = NativeModules.Sensors
//

import { NativeModules, DeviceEventEmitter  } from 'react-native';

const setInitialLatitude = (val) => {
    NativeModules.Sensors.setInitialLatitude(val);
};

const setInitialLongitude = (val) => {
    NativeModules.Sensors.setInitialLongitude(val);
};

const listen = DeviceEventEmitter.addListener('SensorData',  (data) => {});

const start = NativeModules.Sensors.start;

const stop = NativeModules.Sensors.stop;

export default Sensors;
