package com.nearmotion.indoorsensors;

import android.graphics.Camera;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.uimanager.PixelUtil;

import java.util.List;

/**
 * Bridge Methods for react native.
 *
 * Created by Majid Aljishi on 1/14/2018.
 */

public class SensorsModule extends ReactContextBaseJavaModule {

    private final String REACT_CLASS = "Sensors";
    private SensorsDataProvider mSensorDataProvider = null;

    public SensorsModule(ReactApplicationContext reactContext) {
        super(reactContext);
        mSensorDataProvider = new SensorsDataProvider(reactContext);
    }

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @ReactMethod
    public void start() {
        if (mSensorDataProvider != null) {
            mSensorDataProvider.start();
        }
    }

    @ReactMethod
    public void stop() {
        if (mSensorDataProvider != null)
            mSensorDataProvider.stop();
    }

    @ReactMethod
    public void isStarted(final Promise promise) {
        if (!mSensorDataProvider.isStarted()) {
            promise.resolve(false);
        } else {
            promise.resolve(true);
        }
    }

    @ReactMethod
    public void setInitialLatitude(Double val) {
        if (mSensorDataProvider != null)
            mSensorDataProvider.setInitialLatitude(val);
    }

    @ReactMethod
    public void setInitialLongitude(Double val) {
        if (mSensorDataProvider != null)
            mSensorDataProvider.setInitialLongitude(val);
    }

    @ReactMethod
    public void setBearingFix(Float val) {
        if (mSensorDataProvider != null)
            mSensorDataProvider.setBearingFix(val);
    }

    @ReactMethod
    public void disableLowPassFilter() {
        if (mSensorDataProvider != null) {
            mSensorDataProvider.disableLowPass();
        }
    }
}
