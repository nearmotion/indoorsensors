
Indoor Sensors
============================

calculating distance increment latitude and longitude base on sensor information.

Add it to your project
-------------------------

`$ npm install --save git+https://nearmotion@bitbucket.org/nearmotion/indoorsensors.git
`

### Install Manually

Make alterations to the following files:

* `android/settings.gradle`

```gradle
...
include ':indoorsensors'
project(':indoorsensors').projectDir = new File(settingsDir, '../node_modules/indoorsensors/indoorsensors')
```

* `android/app/build.gradle`

```gradle
...
dependencies {
    ...
    compile project(':indoorsensors')
}
```

* register module (in MainApplication.java)

  * For react-native below 0.19.0 (use `cat ./node_modules/react-native/package.json | grep version`)

```java
import com.nearmotion.indoorsensors.SensorsPackage; // <------ add package

public class MainActivity extends Activity implements DefaultHardwareBackBtnHandler {

  ......

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mReactRootView = new ReactRootView(this);

    mReactInstanceManager = ReactInstanceManager.builder()
      .setApplication(getApplication())
      .setBundleAssetName("index.android.bundle")
      .setJSMainModuleName("index.android")
      .addPackage(new MainReactPackage())
      .addPackage(new SensorsPackage())      // <------- add package
      .setUseDeveloperSupport(BuildConfig.DEBUG)
      .setInitialLifecycleState(LifecycleState.RESUMED)
      .build();

    mReactRootView.startReactApplication(mReactInstanceManager, "ExampleRN", null);

    setContentView(mReactRootView);
  }

  ......

}
```

  * For react-native 0.19.0 and higher
```java
import com.nearmotion.indoorsensors.SensorsPackage; // <------ add package

public class MainApplication extends Application implements ReactApplication {
   // ...
    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
        new MainReactPackage(), // <---- add comma
        new SensorsPackage() // <---------- add package
      );
    }
```

Api
----

### Setup
```js
import React, {
  DeviceEventEmitter // will emit events that you can listen to
} from 'react-native';

import { Sensors } from 'NativeModules';
```


```js

Sensors.setInitialLatitude(00.00); 
Sensors.setInitialLongitude(00.00);

Sensors.start();
DeviceEventEmitter.addListener('SensorData', (data) => {
  /**
  * data.type ---> { GPS or Sensor } you can use this for if statement.
  * data.newLatitude
  * data.newLongitude
  * data.currentDistance
  * data.currentAcceleration
  * data.currentSpeed
  * data.currentDirection
  **/
});
Sensors.stop(); // add when you want to stop reading

if (Sensors.isStarted) {
   // Do Somthing
} else {
   // Do Somthing
}


// disable lowPass Filter
Sendors.disableLowPassFilter();

// fix bearing as you wish
Sensors.setBearingFix(0.95F); // must be float value

```